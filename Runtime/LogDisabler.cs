﻿#if DISABLE_LOGS
using UnityEngine;

public static class LogDisabler
{
	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
	static void DisableLog()
	{
        Debug.unityLogger.logEnabled = false;
	}
}
#endif
