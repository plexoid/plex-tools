﻿using System;
using Plex_tools.Runtime.Tools.Singletones;
using UnityEngine;

namespace Plex_tools.Runtime.Tools.BackButton {
    public class BackButton: SingletoneMonoBehaviour<BackButton>
    {
        private int _stoppers;

        public event Action OnBack = () => { };

        public void Disable() {
            _stoppers++;
        }

        public void Enable() {
            if (_stoppers > 0) _stoppers--;
        }

        public void EnableAndClear() { _stoppers = 0; }
        
        private void Update() {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                if (_stoppers > 0) return;
                OnBack();
            }
        }
    }
}