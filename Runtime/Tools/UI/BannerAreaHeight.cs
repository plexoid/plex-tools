using UnityEngine;

namespace Plex_tools.Runtime.Tools.UI
{
    [DefaultExecutionOrder(-100)]
    [RequireComponent(typeof(RectTransform))]
    public class BannerAreaHeight : BannerArea
    {
        protected override void Apply(float size)
        {
            var height = thiz.sizeDelta;
            height.y += size;
            thiz.sizeDelta = height;
        }
    }
}