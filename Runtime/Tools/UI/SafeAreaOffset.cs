using UnityEngine;

namespace Plex_tools.Runtime.Tools.UI
{
	[DefaultExecutionOrder(-100)]
	public class SafeAreaOffset : SafeArea
	{
		public bool up;
	
		protected override void Apply(float top, float bot)
		{
			float offset = 0;

			if (applyTop) offset += top;
			if (applyBottom) offset += bot;

			var position = thiz.anchoredPosition;
			if (up) position.y -= offset;
			else position.y += offset;
			thiz.anchoredPosition = position;
		}
	}
}
