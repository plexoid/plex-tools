using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Plex_tools.Runtime.Tools.UI
{
    [DefaultExecutionOrder(-100)]
    [RequireComponent(typeof(RectTransform))]
    public class BannerArea : MonoBehaviour
    {
        protected RectTransform thiz;
        private bool moved;
        private bool initialized;

        public static readonly PrefsBool NoAds = new PrefsBool("no-ads-plex");
        
        private static float bannerHeight;

        /// <summary>
        /// Call this before first banner area will used!
        /// </summary>
        /// <param name="height">Height of banner</param>
        public static void InitializeBannerHeight(float height)
        {
            bannerHeight = height;
        }

        private void Start() 
        {
            WaitCanvas().Forget();
            NoAds.OnChange += () => WaitCanvas().Forget();
        }
	
        private async UniTaskVoid WaitCanvas()
        {
            var mainCanvas = GetComponentInParent<CanvasScaler>().GetComponent<Canvas>();
            await UniTask.WaitUntil(() => mainCanvas.enabled, cancellationToken: this.GetCancellationTokenOnDestroy());
            UpdateView();
        }

        private void UpdateView()
        {
            var rootTransform = (RectTransform)(GetComponentInParent<CanvasScaler>().transform);
            thiz ??= transform as RectTransform;
            float scale = ScreenSizes.ScreenHeight / rootTransform.rect.height;
            
            if (NoAds.Value)
            {
                if (moved == false) return;
                Apply(-bannerHeight / scale);
                moved = false;
            }
            else
            {
                if (moved) return;
                Apply(bannerHeight / scale);
                moved = true;
            }
        }

        protected virtual void Apply(float size)
        {
            var offsetMin = thiz.offsetMin;
            offsetMin.y += size;
            thiz.offsetMin = offsetMin;
        }
    }
}