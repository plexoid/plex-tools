using UnityEngine;

namespace Plex_tools.Runtime.Tools.UI
{
    [DefaultExecutionOrder(-100)]
    [RequireComponent(typeof(RectTransform))]
    public class BannerAreaOffset : BannerArea
    {
        protected override void Apply(float size)
        {
            var position = thiz.anchoredPosition;
            position.y += size;
            thiz.anchoredPosition = position;
        }
    }
}