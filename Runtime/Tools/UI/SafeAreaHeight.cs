using UnityEngine;

namespace Plex_tools.Runtime.Tools.UI
{
	[DefaultExecutionOrder(-100)]
	public class SafeAreaHeight : SafeArea
	{
		protected override void Apply(float top, float bot)
		{
			float additionHeight = 0;

			if (applyTop) additionHeight += top;
			if (applyBottom) additionHeight += bot;

			var sizeDelta = thiz.sizeDelta;
			sizeDelta.y += additionHeight;
			thiz.sizeDelta = sizeDelta;
		}
	}
}
