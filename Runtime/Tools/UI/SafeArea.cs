using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Plex_tools.Runtime.Tools.UI
{
	[DefaultExecutionOrder(-100)]
	[RequireComponent(typeof(RectTransform))]
	public class SafeArea : MonoBehaviour
	{
		public bool applyTop;
		public bool applyBottom;

		protected RectTransform thiz;
	
		private void Start() 
		{
			WaitCanvas().Forget();
		}
	
		private async UniTaskVoid WaitCanvas()
		{
			var mainCanvas = GetComponentInParent<CanvasScaler>().GetComponent<Canvas>();
			await UniTask.WaitUntil(() => mainCanvas.enabled, cancellationToken: this.GetCancellationTokenOnDestroy());
			UpdateView();
		}

		private void UpdateView()
		{
			if (this == null) return;
			var rootTransform = (RectTransform)(GetComponentInParent<CanvasScaler>().transform);
			thiz = transform as RectTransform;
			float scale = ScreenSizes.ScreenHeight / rootTransform.rect.height;
			Apply(ScreenSizes.TopOffset / scale, ScreenSizes.BottomOffset / scale);
		}

		protected virtual void Apply(float top, float bot)
		{
			if (applyBottom)
			{
				var offsetMin = thiz.offsetMin;
				offsetMin.y += bot;
				thiz.offsetMin = offsetMin;
			}

			if (applyTop)
			{
				var offsetMax = thiz.offsetMax;
				offsetMax.y -= top;
				thiz.offsetMax = offsetMax;
			}
		}
	}
}
