using System;
#if ENABLE_LOGS
using System.Collections.Generic;
using System.Diagnostics;
#endif

namespace Plex_tools.Runtime.Tools
{
    public class Watcher: IDisposable
    {
#if ENABLE_LOGS
        private readonly string tag;
        private static List<Stopwatch> _pool;
        private readonly Stopwatch _stopwatch;

        private Stopwatch GetStopwatch()
        {
            if (_pool == null)
            {
                _pool = new List<Stopwatch>();
            }

            if (_pool.Count == 0)
            {
                _pool.Add(new Stopwatch());
            }
        
            var stopwatch = _pool[0];
            _pool.RemoveAt(0);
            return stopwatch;
        }

        private void ReturnStopwatch(Stopwatch stopwatch)
        {
            _pool.Add(stopwatch);
        }
#endif
    
        public Watcher(string tag)
        {
#if ENABLE_LOGS
            this.tag = tag;
            _stopwatch = GetStopwatch();
            _stopwatch.Start();
#endif
        }

        public void Dispose()
        {
#if ENABLE_LOGS
            _stopwatch.Stop();
            CustomLogger.Log($"[watcher] [{tag}] ms = {_stopwatch.ElapsedMilliseconds}");
            ReturnStopwatch(_stopwatch);
#endif
        }
    }
}
