using System;
using UnityEngine;

namespace Plex_tools.Runtime.Tools.Extensions
{
	public static class TextureExtensions
	{
		public static void Rotate(this Texture2D texture, float angle, bool clockwise)
		{
			int times = (int)((angle + 720) / 90) % 4;
			if (times == 0 && clockwise == false) return;

			int width = texture.width;
			int height = texture.height;

			Color32[] origin = texture.GetPixels32();

			int length = origin.Length;
			Color32[] rotated = new Color32[length];

			// formulas
			int OneRotation(int x, int y) => (width - 1 - x) * height + y;
			int TwoRotations(int x, int y) => (height - y) * width - 1 - x;
			int ThreeRotations(int x, int y) => (x + 1) * height - 1 - y;

			int OneRotationClockwise(int x, int y) => (width - x) * height - 1 - y;
			int TwoRotationsClockwise(int x, int y) => (height - y - 1) * width + x;
			int ThreeRotationsClockwise(int x, int y) => x * height + y;
			int ZeroRotationsClockwise(int x, int y) => y * width + width - 1 - x;

			Func<int, int, int> getRotatedIndex;
			if (clockwise)
			{
				getRotatedIndex = new Func<int, int, int>[] {
					ZeroRotationsClockwise,
					OneRotationClockwise,
					TwoRotationsClockwise,
					ThreeRotationsClockwise
				}[times];
			}
			else
			{
				getRotatedIndex = new Func<int, int, int>[] {
					OneRotation,
					TwoRotations,
					ThreeRotations
				}[times - 1];
			}

			int rotatedIndex, index;
			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					index = y * width + x;
					rotatedIndex = getRotatedIndex(x, y);
					rotated[rotatedIndex] = origin[index];
				}
			}

			if (times % 2 == 1 && width != height)
			{
#if UNITY_2021
				texture.Reinitialize(height, width);
#else
				texture.Resize(height, width);
#endif
			}

			texture.SetPixels32(rotated);
			texture.Apply();
		}
	}
}