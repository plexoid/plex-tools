﻿using System.IO;
using Debug = Plex_tools.Runtime.Tools.CustomLogger;

namespace Plex_tools.Runtime.Tools.Extensions
{
	public static class FileExtensions
	{
		public static void CreateDirectoryIfNull(string path, bool getDirectoryName = false)
		{
			if (string.IsNullOrEmpty(path))
			{
				CustomLogger.LogWarning("Trying creating folder with empty path!");
				return;
			}

			if (getDirectoryName)
			{
				path = Path.GetDirectoryName(path);
				
				if (string.IsNullOrEmpty(path))
				{
					CustomLogger.LogWarning($"Incorrect folder path \"{path}\"!");
					return;
				}
			}
			
			DirectoryInfo info = new DirectoryInfo(path);
			if (!info.Exists)
			{
				info.Create();
			}
		}
	}
}