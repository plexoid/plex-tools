﻿using UnityEngine;

namespace Plex_tools.Runtime.Tools.Extensions
{
    public static class ColorExtensions
    {
        public static Color GetGrayscaleColor(this Color origin, float opacity = 1)
        {
            float value = 0.2627f * origin.r + 0.6780f * origin.g + 0.0593f * origin.b;
            value = Mathf.Lerp(1f, value, opacity);
            return new Color(value, value, value, origin.a);
        }
    }
}