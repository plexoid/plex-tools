using System.Collections.Generic;
using System.Text;

namespace Plex_tools.Runtime.Tools.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveChars(this string s, char a, params char[] extra)
        {
            if (string.IsNullOrEmpty(s)) return s;

            int length = s.Length;
            
            StringBuilder sb = new StringBuilder(length, length);

            var hash = new HashSet<char>();
            hash.Add(a);
            if (extra != null)
            {
                for (int i = 0; i < extra.Length; i++)
                {
                    hash.Add(extra[i]);
                }
            }

            for (int i = 0; i < length; i++)
            {
                if (hash.Contains(s[i])) continue;
                sb.Append(s[i]);
            }
            
            return sb.ToString();
        }
    }
}