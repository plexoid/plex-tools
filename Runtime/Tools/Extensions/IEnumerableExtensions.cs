﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Plex_tools.Runtime.Tools.Extensions
{
	public static class IEnumerableExtensions
	{
		private static Random _random;

		private static int Random(int min, int max)
		{
			if (_random == null) _random = new Random();
			return _random.Next(min, max);
		}

		public static void Shuffle<T>(this T array) where T: IList, new()
		{
			int count = array.Count;
			for (int i = 0; i < count - 1; i++)
			{
				var item = array[i];
				int index = Random(i + 1, count);
				array[i] = array[index];
				array[index] = item;
			}
		}

		public static List<T> GetShuffled<T>(this IEnumerable<T> array)
		{
			var list = array.ToList();
			list.Shuffle();
			return list;
		}

		public static T GetRandom<T>(this IList<T> array)
		{
			int count = array.Count;
			if (count == 0) return default;
			return array[Random(0, count)];
		}

		public static T GetRandom<T>(this ICollection<T> array)
		{
			int count = array.Count;
			if (count == 0) return default;

			int index = Random(0, count);
			using (var link = array.GetEnumerator())
			{
				int i = 0;
				while (link.MoveNext() && i < index) i++;

				return link.Current;
			}
		}

		public static void AddRange<T>(this ICollection<T> target, IEnumerable<T> source)
		{
			if (target == null)
				throw new ArgumentNullException(nameof(target));
			if (source == null)
				throw new ArgumentNullException(nameof(source));
			foreach (var element in source)
				target.Add(element);
		}

		public static void ShuffleListBySeed(this IList list, int seed)
		{
			var random = new Random(seed);
			int count = list.Count;
			for (int i = 0; i < count - 1; i++)
			{
				var item = list[i];
				int index = random.Next(i + 1, count);
				list[i] = list[index];
				list[index] = item;
			}
		}
	}
}