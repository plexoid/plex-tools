﻿using UnityEngine;

namespace Plex_tools.Runtime.Tools
{
    public static class ScreenSizes
    {
        public static readonly float TopOffset;
        public static readonly float BottomOffset;
        public static readonly float ScreenHeight;
        public static readonly float ScreenWidth;
        
        static ScreenSizes()
        {
            Application.targetFrameRate = 300;

            Rect safeArea = Screen.safeArea;

            ScreenHeight = Screen.currentResolution.height;
            ScreenWidth = Screen.currentResolution.width;
            
            TopOffset = ScreenHeight - safeArea.yMax;
            BottomOffset = safeArea.yMin;
        }
    }
}