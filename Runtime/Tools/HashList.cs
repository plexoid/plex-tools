using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Plex_tools.Runtime.Tools
{
	public class HashList<T>: IEnumerable<T>
	{
		private readonly List<T> _list = new List<T>();
		private readonly HashSet<T> _hash = new HashSet<T>();

		public T this[int i] => _list[i];

		public int Count => _list.Count;

		public HashList() { }

		public HashList(IEqualityComparer<T> comparer)
		{
			_hash = new HashSet<T>(comparer);
		}

		public HashList(IEnumerable<T> copy)
		{
			if (copy == null) return;
		
			foreach (var t in copy)
				Add(t);
		}

		public void Add(T t)
		{
			if (_hash.Add(t)) _list.Add(t);
		}

		public bool Remove(T t)
		{
			if (_hash.Remove(t))
			{
				_list.Remove(t);
				return true;
			}
		
			return false;
		}

		public void Insert(int i, T t)
		{
			if (_hash.Add(t)) _list.Insert(i, t);
		}

		public void RemoveAt(int i)
		{
			if (_list.Count > i)
			{
				_hash.Remove(_list[i]);
				_list.RemoveAt(i);
			}
		}

		public void Clear()
		{
			_list.Clear();
			_hash.Clear();
		}

		public bool Contains(T t) { return _hash.Contains(t); }
	
		public T FirstOrDefault(Func<T, bool> predicate)
		{
			return _list.FirstOrDefault(predicate);
		}

		public T[] ToArray()
		{
			return _list.ToArray();
		}

		public int IndexOf(T t) { return _list.IndexOf(t); }

		public IEnumerator<T> GetEnumerator() { return _list.GetEnumerator(); }

		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
	}
}