﻿using System.Collections.Generic;
using UnityEngine;
using Debug = Plex_tools.Runtime.Tools.CustomLogger;

namespace Plex_tools.Runtime.Tools
{
	public interface IDynamicItem
	{
		void OnSetView(int index);
	}

	public class DynamicList<T> where T : IDynamicItem
	{
		private readonly List<T> _list;
		public List<T> Items => _list;
		
		public int Count { get; }

		private int _startIndex;

		private int _sizeOfFullList;

		public DynamicList(List<T> list, int sizeOfFullList)
		{
			_list = list;
			_sizeOfFullList = sizeOfFullList;
			Count = list.Count;
		}

		public void Initialize(int startIndex, int newSizeOfFullList = -1)
		{
			if (newSizeOfFullList > 0)
			{
				_sizeOfFullList = newSizeOfFullList;
			}

			if (_sizeOfFullList > Count)
			{
				_startIndex = Mathf.Clamp(startIndex, 0, _sizeOfFullList - Count);
			}
			else
			{
				_startIndex = 0;
			}

			CustomLogger.Log("[ImageView] Initialize DynamicList");
			UpdateViews();
		}

		public void UpdateViews()
		{
			for (int i = 0; i < Count; i++)
			{
				_list[i].OnSetView(_startIndex + i);
			}
		}

		public bool FromEndToStart()
		{
			if (_startIndex > 0)
			{
				_startIndex--;

				var item = _list[Count - 1];
				_list.RemoveAt(Count - 1);
				_list.Insert(0, item);

				item.OnSetView(_startIndex);
				return true;
			}

			return false;
		}

		public bool FromStartToEnd()
		{
			if (_startIndex + Count < _sizeOfFullList)
			{
				_startIndex++;

				var item = _list[0];
				_list.RemoveAt(0);
				_list.Add(item);

				item.OnSetView(_startIndex + Count - 1);
				return true;
			}

			return false;
		}
	}
}
