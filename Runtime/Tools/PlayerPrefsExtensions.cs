﻿using System;
using UnityEngine;

namespace Plex_tools.Runtime.Tools
{
	public abstract class PrefsValue<T>
	{
		// ReSharper disable once MemberCanBeProtected.Global
		public readonly string Key;
		public T Value => getter();
		
		private T value;

		public event Action OnChange = delegate { };

		private delegate T ValueGetter();

		private ValueGetter getter;

		private T lambda() => value;

		public PrefsValue(string key, T defaultValue = default, bool autoSave = false)
		{
			Key = key;
			getter = () => {
				if (Exists())
				{
					value = Load(defaultValue);	
				}
				else
				{
					value = defaultValue;
					if (autoSave) Save();
				}
				getter = lambda;
				return value;
			};
		}

		public virtual void Set(T v)
		{
#if RT_DEV
		Debug.Log($"[Key: {Key}] set {v} (was {this.value})");
#endif
			this.value = v;
			getter = lambda;
			Save();
			OnChange();
		}

		public bool Exists()
		{
			return PlayerPrefs.HasKey(Key);
		}

		protected abstract void Save();
		protected abstract T Load(T defaultValue = default);
	}

	public class PrefsEnum<T>: PrefsValue<T> where T: Enum
	{
		public PrefsEnum(string key, T defaultValue = default) : base(key, defaultValue) { }

		protected override void Save()
		{
			PlayerPrefs.SetInt(Key, Convert.ToInt32(Value));
		}

		protected override T Load(T defaultValue = default)
		{
			return (T)(object)PlayerPrefs.GetInt(Key, Convert.ToInt32(defaultValue));
		}

		public override void Set(T v)
		{
			if (!this.Value.Equals(v))
				base.Set(v);
		
			if (!Exists()) Save();
		}
	}

	public class PrefsString: PrefsValue<string>
	{
		public PrefsString(string key, string defaultValue = default): base(key, defaultValue) { }

		protected override void Save() => PlayerPrefs.SetString(Key, Value);

		protected override string Load(string defaultValue = default) => PlayerPrefs.GetString(Key, defaultValue);
	}

	public class PrefsInt: PrefsValue<int>
	{
		public PrefsInt(string key, int defaultValue = default): base(key, defaultValue) { }

		protected override void Save() => PlayerPrefs.SetInt(Key, Value);

		protected override int Load(int defaultValue = default) => PlayerPrefs.GetInt(Key, defaultValue);

		public override void Set(int v)
		{
			if (!this.Value.Equals(v))
				base.Set(v);
		
			if (!Exists()) Save();
		}

		public void Increment(int amount = 1)
		{
			Set(Value + amount);
		}

		public void Decrement(int amount = 1)
		{
			Set(Value - amount);
		}
	}

	public class PrefsFloat: PrefsValue<float>
	{
		public PrefsFloat(string key, float defaultValue = default): base(key, defaultValue) { }

		protected override void Save() => PlayerPrefs.SetString(Key, Value.ToString("F20"));

		protected override float Load(float defaultValue = default)
		{
			if (PlayerPrefs.HasKey(Key)) {
				if (float.TryParse(PlayerPrefs.GetString(Key), out float result))
				{
					return result;
				}
			}

			return defaultValue;
		}

		public override void Set(float v)
		{
			if (!this.Value.Equals(v))
				base.Set(v);
		
			if (!Exists()) Save();
		}

		public void Increment(float amount = 1f)
		{
			Set(Value + amount);
		}

		public void Decrement(float amount = 1f)
		{
			Set(Value - amount);
		}
	}

	public class PrefsBool: PrefsValue<bool>
	{
		public PrefsBool(string key, bool defaultValue = default, bool autoSave = false): base(key, defaultValue, autoSave) { }

		protected override void Save() => PlayerPrefs.SetInt(Key, Value ? 1 : 0);

		protected override bool Load(bool defaultValue = default) => PlayerPrefs.GetInt(Key, defaultValue ? 1 : 0) == 1;

		public override void Set(bool v)
		{
			if (!this.Value.Equals(v))
				base.Set(v);
		
			if (!Exists()) Save();
		}
	}

	public class PrefsDateTime: PrefsValue<DateTime>
	{
		public PrefsDateTime(string key, DateTime defaultValue = default): base(key, defaultValue) { }

		protected override void Save()
		{
			PlayerPrefs.SetString(Key, Value.ToBinary().ToString());
		}

		protected override DateTime Load(DateTime defaultValue = default)
		{
			if (PlayerPrefs.HasKey(Key)) {
				long dateLong = long.Parse(PlayerPrefs.GetString(Key));
				return DateTime.FromBinary(dateLong);
			}
		
			return defaultValue;
		}
	}
}