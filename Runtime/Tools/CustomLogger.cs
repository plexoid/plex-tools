using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace Plex_tools.Runtime.Tools
{
    public static class CustomLogger
    {
        [Conditional("ENABLE_LOGS")]
        public static void Log(object message, Object context = null)
        {
            Debug.Log(message, context);
        }
    
        [Conditional("ENABLE_LOGS")]
        public static void LogWarning(object message, Object context = null)
        {
            Debug.LogWarning(message, context);
        }
    
        [Conditional("ENABLE_LOGS")]
        public static void LogError(object message, Object context = null)
        {
            Debug.LogError(message, context);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogException(Exception exception, Object context = null)
        {
            Debug.LogException(exception, context);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogFormat(string format, params object[] args)
        {
            Debug.LogFormat(format, args);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogFormat(Object context, string format, params object[] args)
        {
            Debug.LogFormat(context, format, args);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogWarningFormat(string format, params object[] args)
        {
            Debug.LogWarningFormat(format, args);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogWarningFormat(Object context, string format, params object[] args)
        {
            Debug.LogWarningFormat(context, format, args);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogErrorFormat(string format, params object[] args)
        {
            Debug.LogErrorFormat(format, args);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogErrorFormat(Object context, string format, params object[] args)
        {
            Debug.LogErrorFormat(context, format, args);
        }
    }
}
