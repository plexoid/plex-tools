using System.IO;

namespace Plex_tools.Runtime.Tools.IO
{
    public interface IBinarySave
    {
        void Save(BinaryWriter binaryWriter);
    }
}