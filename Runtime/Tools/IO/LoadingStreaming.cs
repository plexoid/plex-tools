using Cysharp.Threading.Tasks;
using System.IO;
using UnityEngine;

namespace Plex_tools.Runtime.Tools.IO
{
	public static class LoadingStreaming
	{
		public static readonly string StreamingPath = Application.streamingAssetsPath;
		
		public static async UniTask<byte[]> LoadFileBytes(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = Path.Combine(StreamingPath, path);
#if UNITY_EDITOR || UNITY_IOS
			return await LoadingFile.LoadFileBytesAsync(path);
#elif UNITY_ANDROID
			return await LoadingRequest.LoadFileBytesAsync(path);
#endif
		}
		
		public static async UniTask<string> LoadFileString(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = Path.Combine(StreamingPath, path);
#if UNITY_EDITOR || UNITY_IOS
			return await LoadingFile.LoadFileStringAsync(path);
#elif UNITY_ANDROID
			return await LoadingRequest.LoadFileStringAsync(path);
#endif
		}
	}
}