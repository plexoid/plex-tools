using System;
using System.IO;
using Debug = Plex_tools.Runtime.Tools.CustomLogger;

namespace Plex_tools.Runtime.Tools.IO
{
	public static class LoadingBinary
	{
		public static bool SaveBinary(string fileNamePath, IBinarySave data)
		{
			return SaveBinary(fileNamePath, data.Save);
		}

		public static bool SaveBinary(string fileNamePath, Action<BinaryWriter> saver)
		{
			string path = LoadingLocal.CombineToLocalPath(fileNamePath);

			Stream stream = null;
			BinaryWriter binaryWriter = null;
			try
			{
				stream = File.OpenWrite(path);
				binaryWriter = new BinaryWriter(stream);
				saver(binaryWriter);
				return true;
			}
			catch (Exception e)
			{
				CustomLogger.LogException(e);
			}
			finally
			{
				binaryWriter?.Close();
				stream?.Close();
			}

			return false;
		}

		public static bool SaveEmpty(string fileNamePath)
		{
			string path = LoadingLocal.CombineToLocalPath(fileNamePath);

			try
			{
				File.WriteAllBytes(path, Array.Empty<byte>());
				return true;
			}
			catch (Exception e)
			{
				CustomLogger.LogException(e);
			}

			return false;
		}

		public static bool LoadBinary(string fileNamePath, IBinaryLoad data)
		{
			return LoadBinary(fileNamePath, data.Load);
		}

		public static bool LoadBinary(string fileNamePath, Action<BinaryReader> loader)
		{
			string path = LoadingLocal.CombineToLocalPath(fileNamePath);

			if (!File.Exists(path)) return false;

			Stream stream = null;
			BinaryReader binaryReader = null;
			try
			{
				stream = File.OpenRead(path);
				binaryReader = new BinaryReader(stream);
				loader(binaryReader);
				return true;
			}
			catch (Exception e)
			{
				CustomLogger.LogException(e);
			}
			finally
			{
				binaryReader?.Close();
				stream?.Close();
			}

			return false;
		}

		public static bool Exists(string fileNamePath)
		{
			string path = LoadingLocal.CombineToLocalPath(fileNamePath);

			return File.Exists(path);
		}

		public static string[] GetFileSaves(string directory)
		{
			string path = LoadingLocal.CombineToLocalPath(directory);

			return Directory.GetFiles(path);
		}

		public static bool Remove(string fileNamePath)
		{
			string path = LoadingLocal.CombineToLocalPath(fileNamePath);

			if (File.Exists(path))
			{
				File.Delete(path);
				return true;
			}

			return false;
		}

		public static bool DirectoryExists(string directoryNamePath)
		{
			string path = LoadingLocal.CombineToLocalPath(directoryNamePath);

			return Directory.Exists(path);
		}

		public static void CreateDirectory(string directoryNamePath)
		{
			string path = LoadingLocal.CombineToLocalPath(directoryNamePath);

			if (Directory.Exists(path)) return;

			Directory.CreateDirectory(path);
		}
	}
}