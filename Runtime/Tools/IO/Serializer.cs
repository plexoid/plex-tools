﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Debug = Plex_tools.Runtime.Tools.CustomLogger;

namespace Plex_tools.Runtime.Tools.IO
{
	/// <summary>
	/// Exceptions can be.
	/// </summary>
	public static class Serializer
	{
		private static BinaryFormatter _formatter;

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		public static void Setup() { _formatter = new BinaryFormatter(); }

		public static void Serialize<T>(Stream stream, T t) { _formatter.Serialize(stream, t); }

		public static T Deserialize<T>(Stream stream)
		{
			stream.Seek(0, SeekOrigin.Begin);
			T result = (T)_formatter.Deserialize(stream);
			return result;
		}

		public static bool Serialize<T>(string path, T t)
		{
			var stream = File.OpenWrite(path);
			try
			{
				Serialize(stream, t);
			}
			catch (Exception e)
			{
				CustomLogger.LogError($"Error when serializing file type <{typeof(T)}> to path: `{path}`. Exception:\n{e}");
				return false;
			}
			finally
			{
				stream.Close();
			}

			return true;
		}

		public static T Deserialize<T>(string path, T _default = default)
		{
			T result = _default;

			if (File.Exists(path))
			{
				var stream = File.OpenRead(path);
				try
				{
					result = Deserialize<T>(stream);
				}
				catch (Exception e)
				{
					CustomLogger.LogError($"Error when deserializing file type <{typeof(T)}> by path: `{path}`. Exception:\n{e}");
				}
				finally
				{
					stream.Close();
				}
			}

			return result;
		}
	}
}