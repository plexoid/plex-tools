namespace Plex_tools.Runtime.Tools.IO
{
	public interface IBinaryFormatData: IBinaryLoad, IBinarySave { }
}
