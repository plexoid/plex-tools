using Cysharp.Threading.Tasks;
using System.IO;

namespace Plex_tools.Runtime.Tools.IO
{
	public static class LoadingFile
	{
		public static byte[] LoadFileBytes(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			if (File.Exists(path) == false) return null;
			return File.ReadAllBytes(path);
		}

		public static string LoadFileString(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			if (File.Exists(path) == false) return null;
			return File.ReadAllText(path);
		}

		public static async UniTask<byte[]> LoadFileBytesAsync(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			byte[] result = null;
			await UniTask.SwitchToThreadPool();
			result = LoadFileBytes(path);
			await UniTask.SwitchToMainThread();
			return result;
		}

		public static async UniTask<string> LoadFileStringAsync(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			string result = null;
			await UniTask.SwitchToThreadPool();
			result = LoadFileString(path);
			await UniTask.SwitchToMainThread();
			return result;
		}
	}
}