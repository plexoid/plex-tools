using System.IO;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Plex_tools.Runtime.Tools.IO
{
	public static class LoadingLocal
	{
		public static string LocalPath { get;  }

		static LoadingLocal()
		{
#if UNITY_EDITOR
			LocalPath = Application.dataPath.Replace("Assets", "Local");
#else
			LocalPath = Application.persistentDataPath;
#endif
		}

#if UNITY_EDITOR
		[UnityEditor.MenuItem("Tools/Clear All", priority = -2000)]
		private static void ClearLocalFolder()
		{
			if (UnityEditor.EditorUtility.DisplayDialog("Clear Saves", "Clear all PlayerPrefs values and files?", "yes", "cancel") == false) return;
			CustomLogger.Log("Cleared all saves");
			PlayerPrefs.DeleteAll();
			PlayerPrefs.Save();
			string path = Application.dataPath.Replace("Assets", "Local");
			if (Directory.Exists(path))
			{
				Directory.Delete(path, true);
			}
		}
#endif

		public static byte[] LoadFileBytes(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = CombineToLocalPath(path);
			return LoadingFile.LoadFileBytes(path);
		}

		public static string LoadFileString(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = CombineToLocalPath(path);
			return LoadingFile.LoadFileString(path);
		}
		
		public static async UniTask<byte[]> LoadFileBytesAsync(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = CombineToLocalPath(path);
			return await LoadingFile.LoadFileBytesAsync(path);
		}

		public static async UniTask<string> LoadFileStringAsync(string path)
		{
			if (string.IsNullOrEmpty(path)) return null;
			path = CombineToLocalPath(path);
			return await LoadingFile.LoadFileStringAsync(path);
		}

		public static string CombineToLocalPath(string path)
		{
#if ALLOW_EDITOR_CHECK_PATH
			if (Application.isPlaying == false)
			{
				return Path.Combine("Local", path);;
			}
#endif
			return Path.Combine(LocalPath, path);
		}
	}
}
