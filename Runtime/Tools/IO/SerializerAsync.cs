using Cysharp.Threading.Tasks;
using System;
using System.IO;
using Debug = Plex_tools.Runtime.Tools.CustomLogger;

namespace Plex_tools.Runtime.Tools.IO
{
	/// <summary>
	/// Exceptions can be.
	/// </summary>
	public class SerializerAsync
	{
		public static async UniTask Serialize<T>(Stream stream, T t)
		{
			void Action()
			{
				try
				{
					Serializer.Serialize(stream, t);
				}
				catch (Exception e)
				{
					CustomLogger.LogException(e);
				}
			}
			
			await UniTask.RunOnThreadPool(Action);
		}

		public static async UniTask<T> Deserialize<T>(Stream stream)
		{
			T result = default;
			void Action()
			{
				try
				{
					result = Serializer.Deserialize<T>(stream);
				}
				catch (Exception e)
				{
					CustomLogger.LogException(e);
				}
			}
			await UniTask.RunOnThreadPool(Action);
			return result;
		}

		public static async UniTask<bool> Serialize<T>(string path, T t)
		{
			bool success = false;
			void Action()
			{
				success = Serializer.Serialize(path, t);
			}
			await UniTask.RunOnThreadPool(Action);
			return success;
		}

		public static async UniTask<T> Deserialize<T>(string path, T _default = default)
		{
			T result = default;
			void Action()
			{
				result = Serializer.Deserialize(path, _default);
			}
			await UniTask.RunOnThreadPool(Action);
			return result;
		}
	}
}