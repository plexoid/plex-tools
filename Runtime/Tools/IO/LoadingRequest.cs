using Cysharp.Threading.Tasks;
using System;
using UnityEngine.Networking;

namespace Plex_tools.Runtime.Tools.IO
{
	public static class LoadingRequest
	{
		private static void LoadWebRequest(string url, Action<DownloadHandler> callback)
		{
			if (string.IsNullOrEmpty(url))
			{
				callback(null);
				return;
			}
			
			var request = UnityWebRequest.Get(url);
			var operation = request.SendWebRequest();
			operation.completed += asyncOperation => {
				callback(request.downloadHandler);
				request.Dispose();
			};
		}
		
		public static async UniTask<byte[]> LoadFileBytesAsync(string url)
		{
			if (string.IsNullOrEmpty(url)) return null;
			bool isDone = false;
			byte[] result = null;
			LoadWebRequest(url, handler => {
				if (handler != null)
				{
					result = handler.data;
				}
				isDone = true;
			});
			
			await UniTask.WaitUntil(() => isDone);
			return result;
		}
		
		public static async UniTask<string> LoadFileStringAsync(string url)
		{
			if (string.IsNullOrEmpty(url)) return null;
			bool isDone = false;
			string result = null;
			LoadWebRequest(url, handler => {
				if (handler != null)
				{
					result = handler.text;
				}
				isDone = true;
			});
			
			await UniTask.WaitUntil(() => isDone);
			return result;
		}
	}
}