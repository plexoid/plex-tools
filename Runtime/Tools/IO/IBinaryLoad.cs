using System.IO;

namespace Plex_tools.Runtime.Tools.IO
{
    public interface IBinaryLoad
    {
        void Load(BinaryReader binaryReader);
    }
}