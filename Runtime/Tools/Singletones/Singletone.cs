namespace Plex_tools.Runtime.Tools.Singletones
{
    public class Singletone<T> where T : Singletone<T>, new()
    {
        private static T _instance;
        
        public static T Instance
        {
            get
            {
                if (ReferenceEquals(_instance, null))
                {
                    _instance = new T();
                }

                return _instance;
            }
        }
    }
}