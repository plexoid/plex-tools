using UnityEngine;

namespace Plex_tools.Runtime.Tools.Singletones {
    public class SingletoneMonoBehaviour<T> : MonoBehaviour where T: SingletoneMonoBehaviour<T> {
        protected static T _instance;
        
        public static T Instance
        {
            get
            {
                if (ReferenceEquals(_instance, null))
                {
                    var go = new GameObject(typeof(T).Name);
                    _instance = go.AddComponent<T>();
                    DontDestroyOnLoad(go);
                }

                return _instance;
            }
        }
    }
}