#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Plex_tools.Runtime.Tools
{
	public static class AssetGetter
	{
		public static string[] GetAllPathsByType<T>(string pathFilter = null) where T: Object
		{
			var objsGUID = AssetDatabase.FindAssets("t:" + typeof(T).Name);
			var paths = objsGUID.Select(AssetDatabase.GUIDToAssetPath);
			if (string.IsNullOrEmpty(pathFilter)) return paths.ToArray();
			return paths.Where(path => path.Contains(pathFilter)).ToArray();
		}

		public static T[] GetAllObjectsByType<T>(string pathFilter = null) where T: Object
		{
			var paths = GetAllPathsByType<T>(pathFilter);
			return GetAllObjectByPaths<T>(paths);
		}

		public static T[] GetAllObjectByPaths<T>(IEnumerable<string> paths) where T: Object
		{
			if (paths == null) return new T[0];
			return paths.Select(AssetDatabase.LoadAssetAtPath<T>).ToArray();
		}
	}
}

#endif