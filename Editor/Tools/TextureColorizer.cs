using System.IO;
using UnityEditor;
using UnityEngine;

namespace Tools.Editor
{
    public class TextureColorizer: EditorWindow
    {
        private Color32 color;
        
        public static void Colorize(Texture2D texture, Color32 color)
        {
            var pixels = texture.GetPixels32();
            for (int i = 0; i < pixels.Length; i++)
            {
                pixels[i] = new Color32(color.r, color.g, color.b, pixels[i].a);
            }
            texture.SetPixels32(pixels);
        }

        [MenuItem("Tools/Change Texture Color")]
        private static void ChangeColor()
        {
            var colorizer = CreateWindow<TextureColorizer>();
            colorizer.minSize = new Vector2(300, 200);
            colorizer.maxSize = new Vector2(300, 200);
            colorizer.color = new Color32(255, 255, 255, 255);
        }

        private void OnGUI()
        {
            color = EditorGUILayout.ColorField("Color", color);
            GUILayout.Space(50);
            if (GUILayout.Button("Update Texture"))
            {
                string path = EditorUtility.OpenFilePanel("Choose texture", Application.dataPath, "png");
                if (path.Length != 0)
                {
                    var fileContent = File.ReadAllBytes(path);
                    var texture = new Texture2D(1, 1);
                    texture.LoadImage(fileContent);
                    Colorize(texture, color);
                    texture.Apply();
                    File.WriteAllBytes(path, texture.EncodeToPNG());
                }
            }
        }
    }
}